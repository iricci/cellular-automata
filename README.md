1D Cellular Automata
=================

Exam project for the Computational Physics Laboratory exam, Condensed Matter
Physics, University of Trieste.

## References
See [assignment](https://moodle2.units.it/pluginfile.php/566834/mod_folder/content/0/CELLULAR-AUTOMATA-1D-different-rules.pdf?forcedownload=1) and Steven Wolfram's 1983 [paper](https://doi.org/10.1103/RevModPhys.55.601).

## Setup
To properly run the Jupyter notebook containing a brief report (and all of the code needed to reproduce the results):

 1. clone the repository
	```
	git clone https://framagit.org/iricci/cellular-automata.git
	cd cellular-automata
	``` 
 2. set up the virtual environment  by running
	```
	./setup.sh
	```
    with the following dependencies (see `requirements.txt`):
	- numpy
	- scipy
	- matplotlib
	- pltfront
	- f2py-jit
	- jupyter
    > Make sure to have `gfortran` compiler installed.

 3. activate the virtual environment
	```
	. env/bin/activate
	```

 4. open the Jupyter notebook
	```
	jupyter-notebook automata.ipynb
	```
> Note: `Permission denied.` is a frequent error when running shell scripts (or makefiles) from cloned repositories - rightly so, due to safety concerns! Make sure you have the *execute* permission before following the guide. Run `sudo chmod +x setup.sh`.

> Note: all results were obtained using Python 3.10.12.
## Author
Iacopo Ricci

module box
    implicit none
    contains

    subroutine counting(image, box_size, counter)
        integer(8), intent(inout) :: image(:, :)
        integer(8), intent(in) :: box_size
        integer, intent(inout) :: counter
        integer :: i, j, x, y

        x = size(image, 1)
        y = size(image, 2)
        counter = 0
        do i = 1, x, box_size
            do j = 1, y, box_size
                if (any((image(i:min(i+box_size-1, x), j:min(j+box_size-1, y)) .ne. 0))) then
                    counter = counter + 1
                end if
            end do
        end do
    end subroutine counting

    subroutine roll1d(array, idx, rolled)
        integer(8), intent(in) :: idx
        integer(8), intent(inout) :: array(:)
        integer(8), intent(inout) :: rolled(:)

        rolled  = cshift(array, idx)
    end subroutine roll1d
end module box

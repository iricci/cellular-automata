module neighbors
    implicit none
    contains

    subroutine neighbors_generalized(step, z, neighborhood)
        integer(8), intent(inout) :: neighborhood(:, :), step(:)
        integer(8), intent(in) :: z
        integer :: i

        neighborhood(z+1, :) = step
        do i = 1, z
            neighborhood(z+1+i, :) = cshift(step, i)
            neighborhood(z+1-i, :) = cshift(step, -i)
        end do
    end subroutine neighbors_generalized
end module neighbors

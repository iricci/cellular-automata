import matplotlib.pyplot as plt


def cellular_plot(system, title=None, cmap='binary', show=False, figsize=None, fig=None, ax=None):
    """Plot cellular automata system"""

    # External figure handling
    if fig is None and ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    else:
        ax = fig.axes

    # Use in external functions
    if fig is not None and ax is not None:
        ax = ax
        fig = fig

    ax.imshow(system, cmap=cmap, interpolation="nearest")
    if title is not None:
        ax.set_title(title, fontsize=10)
    # Turn off ticks and labels
    ax.xaxis.set_tick_params(labelbottom=False)
    ax.yaxis.set_tick_params(labelleft=False)
    ax.set_xticks([])
    ax.set_yticks([])
    if show:
        plt.show()
    return fig, ax

# Multiplot support for two or more systems


def multiplots(systems, codes, values, sizes, x=1, y=2, figsize=None, fontsize=10):
    # Set colormap
    if values[0] == 2:
        cmap = 'binary'
    else:
        cmap = plt.get_cmap('seismic', lut=values[0])

    fig, axs = plt.subplots(x, y, figsize=figsize)

    # Disable axes
    for ax in axs.ravel():
        ax.set_axis_off()

    for l in list(zip(systems, codes, values, sizes, axs.ravel())):
        l[4].autoscale_view('tight')
        l[4].imshow(l[0], cmap=cmap, interpolation="nearest")
        l[4].set_title(f'Rule {l[1]}, k={l[2]}, z={l[3]}', fontsize=fontsize)
        # Turn off ticks and labels
        l[4].xaxis.set_tick_params(labelbottom=False)
        l[4].yaxis.set_tick_params(labelleft=False)
        l[4].set_xticks([])
        l[4].set_yticks([])
        # Turn on axes one by one if data is provided
        l[4].set_axis_on()
    fig.tight_layout()
    plt.show()

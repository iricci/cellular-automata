import os
import numpy as np
import matplotlib.pyplot as plt
import warnings
from f2py_jit import jit

from src.plot import cellular_plot

# Initialize F90 module
_f90 = jit(os.path.join(os.path.dirname(__file__), 'neighbors.f90'), inline=True, flags="-v -O3 -ffast-math")

def _neighbors_generalized(row, z):
    """Compute matrix of neighbors for a given row of cells"""
    # Repeat array to accomodate all rolls
    array = np.tile(row, (2*z+1, 1))

    # Compute roll indices for each line
    indices = np.arange(-z, z+1)[::-1]

    # Generate meshgrid of rows and column indices of starting array
    rows, column_indices = np.ogrid[:array.shape[0], :array.shape[1]]

    # Shift negative indices to positive values
    indices[indices < 0] += array.shape[1]

    # Shift column indices according to rolls indicated by `indices`
    column_indices = column_indices - indices[:, np.newaxis]

    return array[rows, column_indices]


class Automata(object):
    def __init__(self, ncell, nsteps, neigh_size=1, values=2, code=90, seed=42424, kind='nearest', md5=False, output=None, verbose=False):

        self.ncell = ncell
        """Number of cells"""
        self.nsteps = nsteps
        """Number of simulation steps"""
        self.values = values
        """Number of states for each cell. Default is 2 (i.e. {0, 1})"""
        self.current_step = 0
        """Current simulation step"""
        self.cells = np.zeros([nsteps+1, ncell], dtype=int)
        """Cells, for each expected time step"""
        # NOTE: this setup is risky: nstep is a fixed variable. More steps
        #       could be performed by extending self.cells
        #       yet, this should be more computationally efficient
        self.code = code
        """Decimal rule number"""
        self.rule = []
        """Update rule for each neighborhood"""
        self.verbose = verbose
        """Enable verbose output"""
        self.seed = seed
        """Seed for the RNG"""
        self.rng = np.random.default_rng(seed)
        """Random number generator for initialization"""
        self.lang = 'f90'
        """Language for neighbor computation"""
        self.kind = kind
        """
        Type of rule number conversion.
        `nearest`: neighborhood size is 1. `code` is a rule number.
        `general`: neighborhood size is z. `code` is a totalized code, labeling the specific update rule used.
        """
        self.size = neigh_size
        """Neighborhood size"""
        self.probability = None
        """Probabilities for each cell value for benchmark generation"""

        # Force size to 1 if nearest neighbors are chosen
        if self.kind == 'nearest':
            self.size = 1
            self.values = 2
            warnings.warn("NN interaction using Wolfram numbering is available with binary cells.")

        # Output directory handling
        self.output_dir = None
        """Output directory for simulation results"""
        self.output_file = None
        """Output file name for simulation results"""
        self.output = None
        """Output file for simulation results"""
        self.md5 = md5
        """Generate MD5 hash of output file and use as filename"""

        if output is not None:
            self.output_dir = output
            if not os.path.exists(self.output_dir):
                os.makedirs(self.output_dir)
            # Sanity check
            assert os.path.isdir(self.output_dir), f'output directory is not a directory, {self.output_dir}'

    def benchmark(self, probability=None):
        """Generate a disordered, uncorrelated benchmark configuration"""

        # Uniform probability if none is specified
        if self.probability is None:
            self.probability = [1/self.values]*self.values
        self.cells = self.rng.choice(np.arange(0, self.values), [self.nsteps+1, self.ncell], p=self.probability)

    def initialize(self, idx=None):
        """Set up initial configuration"""

        # No initialization needs to be performed if class is in benchmark mode
        if self.kind == 'benchmark':
            return

        # Set to 1 all specified cells
        if idx is None:
            # Random initialization
            self.cells[0] = self.rng.integers(low=0, high=self.values-1, endpoint=True, size=self.ncell)
        else:
            # Sanity check
            assert np.all(np.array(idx) < self.ncell+1), "initialized bits are out of bounds"
            self.cells[0][idx] = 1

        if self.verbose:
            print("Initialization:")
            print(self.cells[0])

    def neighbors_generalized(self, row, z):
        if self.lang == 'py':
            return _neighbors_generalized(row, z)
        elif self.lang == 'f90':
            neighborhood = np.zeros((2*z+1, self.ncell), dtype=int, order='F')
            row = np.asfortranarray(row)
            _f90.neighbors.neighbors_generalized(row, self.size, neighborhood)
            return neighborhood

    def get_rule(self):
        """Get rule number/totalized code in binary and perform sanity check"""
        # No rule has to be generated if class is in benchmark mode
        if self.kind == 'benchmark':
            self.rule = None
            return

        # Get expected number of configurations
        if self.kind == 'nearest':
            expected = self.values**(2*self.size+1)
        elif self.kind == 'general':
            expected = (2*self.size+1)*(self.values-1)

        # Convert rule in the desired base
        rule_base_raw = np.base_repr(self.code, self.values, 0)
        # Pad appropriately to number of expected configurations
        padding = abs(expected - len(rule_base_raw))+1
        rule_base = '0'*padding + rule_base_raw
        # Transform rule to array and sort accordingly
        self.rule = np.array([int(_) for _ in rule_base], dtype=np.int8)[::-1]

        # Sanity check: convert rule back to decimal
        if self.values == 2:
            converted_rule = int("0b"+"".join(str(_) for _ in self.rule[::-1]), 2)
            assert self.code == converted_rule, f"rule number doesn't match binary conversion, {self.code} != {converted_rule}"

        if self.verbose:
            print(f'Number of expected configurations: {expected}')
            print(f'Number of available configuration: {len(self.rule)}')
            print(f'Rule: {self.rule}')

    def step(self, step=None):
        """Evolve the system by one step"""

        # No step has to be run if class is in benchmark mode
        if self.kind == 'benchmark':
            return

        # Current step
        if step is None:
            current = self.current_step
        cells = self.cells[current]

        # Compute neighborhood
        # NOTE: this approach is more time efficient, for it uses vectorization
        #       via Numpy advanced slicing, at the expense of greater memory use.
        neighborhood = self.neighbors_generalized(cells, self.size)

        # NOTE: Strategy isn't worth it; a simple 'if' is sufficient
        if self.kind == 'nearest':
            # Sanity check
            assert self.size == 1, 'neighborhood size is not 1 (this should NOT happen)'
            # Get decimal index corresponding to each NN neighborhood
            indices = np.left_shift(neighborhood[0], 2) + np.left_shift(neighborhood[1], 1) + np.left_shift(neighborhood[2], 0)

        elif self.kind == 'general':
            # Get neighbors
            neighborhood = self.neighbors_generalized(cells, self.size)
            # Sum over neighborhoods
            indices = np.sum(neighborhood, axis=0)

        # Get update rules
        cells_new = self.rule[indices]
        self.cells[current+1] = cells_new

        # DEBUG
        if self.verbose:
            print(f'DEBUG: step {self.current_step}')
            print(neighborhood)
            print(cells)
            print(cells_new)

    def run(self):
        """Run the simulation for the specified number of steps"""
        # Run simulation
        if self.kind != 'benchmark':
            for _ in range(self.nsteps):
                self.step()
                self.current_step += 1

        if self.verbose:
            print("Final configuration:")
            print(self.cells)

    def save(self):
        """Save simulation data to file"""
        if self.output_dir is None:
            raise IOError('output directory has not been specified.')

        # Fetch metadata, i.e. non-trivial initialization parameters (rule number, etc.)
        # NOTE: initial configuration, number of cells and simulation steps can all be gathered from the system trajectory
        #       `ncell`, `nsteps`, `values` can all be constructed from finished trajectory 
        metadata = {'code': self.code, 'neigh_size': self.size, 'kind': self.kind, 'seed': self.seed, 'values': self.values}
        header = ['# ' + str(metadata) + '\n', '# final configuration \n']

        # Generate filename
        # TODO: add hash table
        # Get output file name
        if self.output_file is None:
            self.output_file = f'{self.kind}_{self.code}_{self.values}_{self.size}.dat'
        if self.kind == 'benchmark':
            self.output_file = f'{self.kind}_{self.values}_{self.size}.dat'

        # Set full output path
        self.output = os.path.join(self.output_dir, self.output_file)
        # Write data to file
        with open(self.output, 'w') as f:
            f.writelines(header)
            np.savetxt(f, self.cells, fmt='%d')

        # MD5 hash renaming
        if self.md5:
            import hashlib
            with open(self.output, "rb") as f:
                data = f.read()
            md5 = hashlib.md5(data).hexdigest()
            os.rename(self.output, os.path.join(self.output_dir, md5+'.dat'))
            self.output = os.path.join(self.output_dir, md5+'.dat')

    def show(self, system=None, fig=None, title=True, show=True, figsize=None):
        """Show system after evolution"""
        # Set colormap
        if self.values == 2:
            cmap = 'binary'
        else:
            cmap = plt.get_cmap('seismic', lut=self.values)

        # External plot handling
        if system is None:
            system = self.cells

        title = f'Rule {self.code}, k={self.values}, z={self.size}'

        cellular_plot(system, title=title, cmap=cmap, show=show, figsize=figsize, fig=fig)

    def do(self, show=True, save=True, figsize=None, idx=None):
        """Initialize system and perform simulation"""

        if self.kind != 'benchmark':
            self.get_rule()
            self.initialize(idx=idx)
            self.run()
        else:
            self.benchmark(self.probability)

        if show:
            self.show(figsize=figsize)
        if save:
            self.save()

    def clear(self, nsteps=None, seed=None, md5=False, verbose=False):
        """
        Clear every system variable
        Note: the only initialization parameters that can be changed are `nsteps`, `seed`, ``
        """

        if self.kind == 'benchmark':
            return

        if nsteps is not None:
            self.nsteps = nsteps
        self.current_step = 0
        self.cells = np.zeros([self.nsteps+1, self.ncell], dtype=int)
        self.verbose = verbose
        self.seed = seed
        self.rng = np.random.default_rng(seed)

        self.output_file = None
        self.output = None
        self.md5 = md5

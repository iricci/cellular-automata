import os
import ast
import warnings
import numpy as np
from f2py_jit import jit


def _box_counting(image, box_size):
    # Function to perform box counting on the image
    count = 0
    for i in range(0, np.shape(image)[0], box_size):
        for j in range(0, np.shape(image)[1], box_size):
            if np.any(image[i:i+box_size, j:j+box_size]):
                count += 1
    return count

# Initialize F90 module
_f90 = jit(os.path.join(os.path.dirname(__file__), 'box.f90'), inline=True, flags="-v -O3 -ffast-math")


class Analysis:
    """Perform analysis of cellular automata trajectory"""

    def __init__(self, inp):
        self.input = inp
        """Trajectory to load"""
        assert os.path.isfile(self.input), f'input trajectory is not a file, {self.input}'
        # self.density = []
        """Density time series for each cell value"""
        self.correlation = []
        """2-point autocorrelation function time series for each cell value"""
        self.hamming = []
        """Hamming distance between consecutive rows"""
        self.counts = None
        """Counts of nonzero cells"""
        self.fractal = None
        """Fractal dimension of the simulation"""
        self.fractal_stdev = None
        """Standard deviation for fractal dimension"""
        self.lang = 'f90'
        """Language used to compute fractal dimension"""

    def table(self):
        """Generate table of available simulations"""
        pass

    def read(self):
        """Load simulation from file"""
        with open(self.input, 'r') as f:
            raw_header = f.readline()
            self.cells = np.genfromtxt(f, dtype=int)
        header = ast.literal_eval(raw_header[2:])
        # Set attributes
        self.code = header['code']
        self.size = header['neigh_size']
        self.kind = header['kind']
        self.seed = header['seed']
        self.values = header['values']
        self.ncell = np.shape(self.cells)[1]
        # NOTE: steps should exclude step 0, i.e. starting config
        self.nsteps = np.shape(self.cells)[0] - 1

        # Set up RNG
        self.rng = np.random.default_rng(self.seed)
        """Random number generator"""

    @property
    def values_visited(self):
        """
        Get maximum number of bit values found in simulation
        Note: it could be useful to analyze configuration space exploration
        """
        # Remove initialization row from cells
        cells = self.cells[1:]
        return len(np.unique(cells))

    # WIP
    def triangles(self):
        """Detect (upside down) triangles and count them"""
        raise NotImplementedError('WIP')
        # Check for streaks of equal values
        # Check next lines, inside boundaries of starting equal values, for a shrinking streak of equal values
        # Iterate to vertex (i.e. streak in {1, 2})

    # Methods and properties

    def density_step(self, step):
        """
        Compute density of flipped sites in step.
        Returns `density` array, containing a value for each possible cell value, in order
        """
        # Initialize to zeros density array
        density = np.zeros(self.values)
        self.counts_step = np.zeros(self.values)

        # Get unique elements and counts in row
        items, counts = np.unique(step, return_counts=True)

        # Compute density
        density[items] = counts/self.ncell

        # Store counts
        self.counts_step[items] = counts
        self.counts_step = list(self.counts_step)
        return list(density)

    @property
    def density(self):
        """
        Compute density at each step.
        Density is given by the total number of flipped sites at a given time
        step divided by the current grid size.
        """
        self.counts = np.zeros((self.nsteps+1, self.values), int)
        for idx, row in enumerate(self.cells):
            item, count = np.unique(row, return_counts=True)
            self.counts[idx][item] = count

        density = np.cumsum(self.counts, axis=0).T/np.tile(np.arange(1, self.nsteps+2)*self.ncell, (self.values, 1))

        self.density_avg = list(1/self.nsteps * np.sum(density, axis=1))
        """Compute average density for each bit value, over all simulation"""
        return density

    @property
    def nonzero_cum(self):
        """Compute cumulative time series of nonzero states"""
        # Sanity check
        assert self.counts is not None, 'counts has not been computed yet. '

        return list(np.cumsum(np.delete(self.counts, 0, 1), dtype=int))

    def correlation_step(self, step):
        """Compute 2-point autocorrelation function over step"""

        # Initialize
        correlation = []
        # Define site conversion
        site = lambda x: np.piecewise(x, [x == 0, x == 1], [-1, 1])
        # Apply site function to step
        step = site(step)
        # Define shifts and iterate over them
        r = np.arange(0, self.ncell)
        for ri in r:
            # Shift row with PBCs
            if self.lang == 'py':
                shifted = np.roll(step, -ri)
            elif self.lang == 'f90':
                shifted = np.zeros(np.shape(step), dtype=int, order='F')
                _f90.box.roll1d(step, -ri, shifted)

            # Compute terms of acf
            avg_product = (1/self.ncell) * np.sum(step[ri:]*shifted[ri:])
            avg_step = (1/self.ncell) * np.sum(step)
            avg_shifted = (1/self.ncell) * np.sum(shifted[ri:])
            correlation.append(avg_product - avg_step*avg_shifted)
        return correlation

    def hamming_step(self, step1, step2):
        """Compute Hamming distance between two rows"""
        return np.sum(np.not_equal(step1, step2))

    def fractal_dimension(self, box_sizes=[2, 4, 8, 16, 32, 64, 128]):
        """Compute fractal dimension of a simulation"""

        # Use box counting
        def box_counting(image, box_size):
            if self.lang == 'py':
                return _box_counting(image, box_size)
            elif self.lang == 'f90':
                counter = np.array(0, dtype=int)
                _f90.box.counting(np.asfortranarray(image), box_size, counter)
                return counter

        counts = []
        for box_size in box_sizes:
            count = box_counting(self.cells, box_size)
            counts.append(count)

        # The fractal dimension is the OPPOSITE of the fitting coefficient
        params = np.polyfit(np.log(box_sizes), np.log(counts), 1, cov=True)
        self.fractal = -params[0][0]
        self.fractal_stdev = np.sqrt(params[1][0, 0])
        self.box_counts = list(zip(box_sizes, counts))
        """Values inside boxes in box counting method for fractal dimension calculation"""

    def analyze(self, simulation=None, **kwargs):
        """Perform analysis over all steps"""
        # Handle user provided simulation, e.g. benchmark
        if simulation is None:
            simulation = self.cells

        # Run analysis over all steps
        for idx, step in enumerate(simulation):
            # Compute Hamming distance between steps
            if idx == 0:
                self.hamming.append(np.nan)
            else:
                self.hamming.append(self.hamming_step(simulation[idx-1], step))
            # Do not compute acf if values != 2
            if self.values == 2:
                self.correlation.append(self.correlation_step(step))
                self.fractal_dimension()
            else:
                warnings.warn(f'No acf available yet for {self.values} values.')

        # Compute density
        _ = self.density

    def results(self):
        """Show analysis results"""

        print(f'Expected bit values: \t {self.values}')
        print(f'Measured bit values: \t {self.values_visited}')
        print(f'Fractal dimension: \t {self.fractal} ± {self.fractal_stdev}')
        print(f'Avg. density: \t\t {self.density_avg}')

    def do(self, verbose=False, **kwargs):
        """Perform complete analysis set and save data"""
        self.read()
        self.analyze(**kwargs)
        if verbose:
            self.results()
